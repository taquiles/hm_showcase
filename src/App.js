import React from "react";
import {inject, observer}  from 'mobx-react';
import { Table} from "antd";

import "antd/dist/antd.css";
import "./index.css";

const App = inject('columnsStore', 'dataStore')(observer(({ columnsStore, dataStore }) => { 
    return (
      <div>
        <Table rowKey="owner" columns={columnsStore.columns} dataSource={dataStore.data} />
      </div>
    );
  }));


export default App;
// src/views/Crud.js
import React from 'react'
import { inject } from 'cans'
import {
  Table,
  Button,
  Divider
} from 'antd'

var geocoder = require("google-geocoder");
var geo = geocoder({
  key: "AIzaSyBOdqV4lG0h3wIOUuZ-_QGaX-vSmtjg9HM"
});

const DataTable = inject(({ models }) => {

    const columns = [
        {
          title: "Owner",
          dataIndex: "owner",
          key: "owner",
          render: text => <a href="#">{text}</a>
        },
        {
          title: "Address",
          dataIndex: "address",
          key: "address",
          render: (text, record) => (
            <span>
              <p>{text.line1 ? text.line1 : ""}</p>
              <p>{text.line2 ? text.line2 : ""}</p>
              <p>{text.line3 ? text.line3 : ""}</p>
              <p>{text.line4 ? text.line4 : ""}</p>
              {text.postCode}
              <Divider type="vertical" />
              {text.city}
              <Divider type="vertical" />
              {text.country}
            </span>
          )
        },
        {
          title: "Service Area",
          dataIndex: "address",
          key: "servicearea",
          render: (text, record) => {
            let latitude;
            let longitude;
            // Get the full address out of text object
            let full_address="";
            // for (const k of Object.keys(text)) {
            //  full_address = full_address + text[k] + " ";
            // }
            
            full_address = text.line1 + " " + text.line4 + " " + text.postCode + " "+ text.city + " " + text.country;
            console.log ('full address >>>>', full_address);
      
            geo.find(full_address,
              function(err, res) {
                // process response object
                console.log("err-", err);
                console.log("res-", res);
                latitude = res[0].location.lat;
                longitude = res[0].location.lng;
              }
            );
            
            return (
              <span>
                <p>{latitude}</p>
                <p>{longitude}</p>
              </span>
            );
          }
        },
        {
          title: "Airbnb Id",
          dataIndex: "airbnbId",
          key: "airbnbId"
        },
        {
          title: "Number Of Bedrooms",
          dataIndex: "numberOfBedrooms",
          key: "numberOfBedrooms"
        },
        {
          title: "Number Of Bathrooms",
          dataIndex: "numberOfBathrooms",
          key: "numberOfBathrooms"
        },
        {
          title: "income Generated",
          dataIndex: "incomeGenerated",
          key: "incomeGenerated"
        }
      ];


  return (
    <div>
      <Button loading={models.rest.properties.loading.index} onClick={models.rest.properties.index}>Fetch Posts</Button>
      <Table
        rowKey='id'
        loading={models.rest.properties.loading.index}
        columns={columns}
        dataSource={models.rest.properties.data.index}
      />
    </div>
  )
})
const Crud = () => {
  return (
    <div>
      <DataTable />
    </div>
  )
}
export default Crud
import React from 'react'
import {observable} from 'mobx';
import { Table, Icon, Divider } from "antd";

var googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyBOdqV4lG0h3wIOUuZ-_QGaX-vSmtjg9HM'
});

const LatitudeCenterCity = 51.5073835;
const LongitudeCenterCity = -0.1277801;
const Radius = 20000; 

class RenderLatLng extends React.Component {

  state = {lat: 0, lng: 0};

  distanceFromCenter (lat, lng) {
    var ky = 40000 / 360;
    var kx = Math.cos(Math.PI * LatitudeCenterCity / 180.0) * ky;
    var dx = Math.abs(LongitudeCenterCity - lng) * kx;
    var dy = Math.abs(LatitudeCenterCity - lat) * ky;
    return Math.sqrt(dx * dx + dy * dy)
  }

  getLatLng (record) {
    let full_address = record.address.line1 + " " + 
      record.address.line4 + " " + 
      record.address.postCode + " "+ 
      record.address.city + " " + 
      record.address.country;
    
    googleMapsClient.geocode({
      address: full_address
    }, function(err, response) {
      if (!err) {
        console.log(response.json.results);
        this.setState({lat: response.json.results[0].geometry.location.lat});
        this.setState({lng: response.json.results[0].geometry.location.lng});
      }
    }.bind(this));
  }

  componentDidMount() {
    this.getLatLng (this.props.record);
    // this._interval = setInterval(() => this.setState({time: this.getTime()}), 50);
  }

  render() {
    let textServiceArea;
    let distance;

    distance= this.distanceFromCenter(this.state.lat, this.state.lng);
    if (distance<=Radius) {
      textServiceArea= 'within ServiceArea (' + distance.toFixed(2) +' km)';
    } else {
      textServiceArea="";
    }
    return (
      <span>
        {textServiceArea}        
      </span>
    )
  }
}

class ColumnsStore {  
  
  constructor() {
    this.columns = observable([
			{
				title: "Owner",
				dataIndex: "owner",
				key: "owner",
				render: text => <a href="#">{text}</a>
      },
      {
        title: "Address",
        dataIndex: "address",
        key: "address",
        render: (text, record) => (
            <span>
            <p>{text.line1 ? text.line1 : ""}</p>
            <p>{text.line2 ? text.line2 : ""}</p>
            <p>{text.line3 ? text.line3 : ""}</p>
            <p>{text.line4 ? text.line4 : ""}</p>
            {text.postCode}
            <Divider type="vertical" />
            {text.city}
            <Divider type="vertical" />
            {text.country}
            </span>
        )
      },
      {
        title: "Distance From Center",
        dataIndex: "latlng",
        key: "latlng",
        render:  (text, record) => (
          <RenderLatLng record={record}/>
        )
      },
      {
        title: "Airbnb Id",
        dataIndex: "airbnbId",
        key: "airbnbId"
      },
      {
        title: "Number Of Bedrooms",
        dataIndex: "numberOfBedrooms",
        key: "numberOfBedrooms"
      },
      {
        title: "Number Of Bathrooms",
        dataIndex: "numberOfBathrooms",
        key: "numberOfBathrooms"
      },
      {
        title: "income Generated",
        dataIndex: "incomeGenerated",
        key: "incomeGenerated"
      }
    ]);    
  }
}


const columnsStore = new ColumnsStore();

export default columnsStore;
export { ColumnsStore };
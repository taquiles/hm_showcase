import React from "react";
import ReactDOM from "react-dom";
import {Provider}  from 'mobx-react';

import App from "./App";

import columnsStore from './Stores/ColumnsStore';
import dataStore from './Stores/DataStore';

const stores = { columnsStore, dataStore };

ReactDOM.render(
    <Provider { ...stores }>
        <App />
    </Provider>,
    document.getElementById("root")
)